import {useState} from 'react'


import {Card,Row,Col,Button} from 'react-bootstrap'




export default function CourseCard({course}) {

	const {name, description, price} = course

	const [count,setCount] = useState(0)

	const [seats,setSeat] = useState(30)

	function enroll() {
		if(seats>0)
		{
			setCount(count + 1)
			setSeat(seats - 1)
		}else{
			alert("No more Seats")
		}
	}



	console.log(useState)

	return (
		<Row className="mt-3 mb-3">
		<Col xs={12} md={12}>
		<Card className="cardHighlight p-3">
		<Card.Body>
		<Card.Title>
		<h2>{name}</h2>
		</Card.Title>
		<Card.Subtitle>
		<strong>Description: </strong>
		</Card.Subtitle>
		<Card.Text>
		{description}
		</Card.Text>
		<Card.Subtitle>
		Price
		</Card.Subtitle>
		<Card.Text>
		PHP {price}
		</Card.Text>
		<Card.Text>
		Enrollees: {count}
		</Card.Text>
		<Card.Text>
		Available Seats: {seats}
		</Card.Text>
		<Button className="bg-primary" onClick={enroll}>Enroll</Button>
		</Card.Body>
		</Card>
		</Col>
		</Row>
		)
}